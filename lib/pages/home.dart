import 'package:flutter/material.dart';
import 'package:mapbox_maps/widgets/full_screen_map.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FullScreenMap(),
    );
  }
}
