import 'package:flutter/material.dart';
import 'package:mapbox_gl/mapbox_gl.dart';

class MapProvider with ChangeNotifier {
  Map<String, String> _mapStyles = {
    'basic-game': 'mapbox://styles/josefnieto/cklyai4j32u8j17msg0gmw83d',
    'streets-salmon': 'mapbox://styles/josefnieto/cklyalwlc3y1m17nytcy0t6gz',
  };

  final LatLng centerPosition = LatLng(3.5371398, -76.3102515);
  String _mapStyle = 'streets-salmon';
  MapboxMapController _mapController;

  get mapStyle => _mapStyles[_mapStyle];

  get mapController => _mapController;

  set mapController(controller) {
    _mapController = controller;
    notifyListeners();
  }

  void changeMapStyle() {
    String tempStyle = _mapStyle;

    _mapStyles.forEach((key, style) {
      if (_mapStyle != key) {
        tempStyle = key;
      }
    });

    _mapStyle = tempStyle;
    notifyListeners();
  }
}
