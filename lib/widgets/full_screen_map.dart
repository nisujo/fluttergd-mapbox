import 'dart:typed_data';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart' as dotenv;
import 'package:http/http.dart' as http;
import 'package:mapbox_gl/mapbox_gl.dart';
import 'package:mapbox_maps/providers/map_provider.dart';
import 'package:provider/provider.dart';

class FullScreenMap extends StatefulWidget {
  FullScreenMap({
    Key key,
  }) : super(key: key);

  @override
  _FullScreenMapState createState() => _FullScreenMapState();
}

class _FullScreenMapState extends State<FullScreenMap> {
  final String _accessToken = dotenv.env['MAPBOX_TOKEN'];
  MapProvider mapProvider;

  @override
  Widget build(BuildContext context) {
    mapProvider = Provider.of<MapProvider>(context);

    return SafeArea(
      child: Stack(
        children: [
          MapboxMap(
            styleString: mapProvider.mapStyle,
            accessToken: _accessToken,
            onMapCreated: _onMapCreated,
            onStyleLoadedCallback: onStyleLoadedCallback,
            initialCameraPosition: CameraPosition(
              target: mapProvider.centerPosition,
              zoom: 14.0,
            ),
          ),
          Positioned(
            bottom: 10.0,
            right: 10.0,
            child: MapFloatingActions(),
          )
        ],
      ),
    );
  }

  void _onMapCreated(MapboxMapController controller) {
    mapProvider.mapController = controller;
  }

  void onStyleLoadedCallback() {
    addImageFromAsset(
      "assetImage",
      "assets/images/custom-icon.png",
    );
    addImageFromUrl(
      "networkImage",
      Uri.parse("https://via.placeholder.com/50.png/09f/fff"),
    );
  }

  Future<void> addImageFromAsset(String name, String assetName) async {
    final ByteData bytes = await rootBundle.load(assetName);
    final Uint8List list = bytes.buffer.asUint8List();
    return mapProvider.mapController.addImage(name, list);
  }

  Future<void> addImageFromUrl(String name, Uri uri) async {
    var response = await http.get(uri);
    return mapProvider.mapController.addImage(name, response.bodyBytes);
  }
}

class MapFloatingActions extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MapProvider mapProvider = Provider.of<MapProvider>(context);
    MapboxMapController mapController = mapProvider.mapController;

    return Column(
      children: [
        FloatingActionButton(
          mini: true,
          child: Icon(Icons.sentiment_very_dissatisfied),
          onPressed: () {
            mapController.addSymbol(
              SymbolOptions(
                geometry: mapProvider.centerPosition,
                textField: 'Simbolo',
                textOffset: Offset(0, 2),
                // iconImage: 'airport-11',
                // iconImage: 'networkImage',
                iconImage: 'assetImage',
                iconSize: 1.0,
              ),
            );
          },
        ),
        FloatingActionButton(
          mini: true,
          child: Icon(Icons.zoom_out),
          onPressed: () => mapController.animateCamera(CameraUpdate.zoomOut()),
        ),
        SizedBox(height: 5.0),
        FloatingActionButton(
          mini: true,
          child: Icon(Icons.zoom_in),
          onPressed: () => mapController.animateCamera(CameraUpdate.zoomIn()),
        ),
        SizedBox(height: 5.0),
        FloatingActionButton(
          child: Icon(Icons.add_to_home_screen),
          onPressed: () => mapProvider.changeMapStyle(),
        ),
      ],
    );
  }
}
