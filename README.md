# Mapbox Maps

Aplicación de prueba para usar los mapas de MapBox.

Al descargar el repositorio, se debe copiar el archivo **.env.example** a **.env** y llenar los datos de configuración con los datos pertinentes.

### Enlaces de documentación

- https://mapbox.com/
- https://studio.mapbox.com/
- https://github.com/tobrun/flutter-mapbox-gl
- https://github.com/mapbox/mapbox-gl-styles

![Screenshot](screenshot.png "Screenshot.png")